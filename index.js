const {DisconnectReason, useSingleFileAuthState} = require('@adiwajshing/baileys')
const makeWASocker = require('@adiwajshing/baileys').default;
const axios = require('axios')


const startSock = () => {
    const {state, saveState} = useSingleFileAuthState('./auth.json')
    const sack = makeWASocker({
        printQRInTerminal: true,
        auth : state
    })

    // Mengevcek koneksi WA
    sack.ev.on('connection.update', function (update, connection2) {
        let _a, _b;
        let connection = update.connection, lastDisconnect = update.lastDisconnect;
        if (connection == 'close') {
            if (((_b = (_a = lastDisconnect.error) === null 
                    || _a === void 0 ? void 0 : _a.output ) === null
                    || _b === void 0 ? void 0 : _b.statusCode ) !== DisconnectReason.loggedOut) {
                        startSock()
                    }
                    
        } else {
            console.log('connection closed', update);
        }
    });

    sack.ev.on('creds.update', saveState);



    sack.ev.on('messages.upsert', async m => {
        const msg = m.messages[0];
        
        if(!msg.key.fromMe && m.type === 'notify'){
            
            console.log("COBA 1 : "+msg.key.remoteJid.replace('@s.whatsapp.net', ''));
            console.log(msg.message.conversation);
            console.log(msg.message.buttonsResponseMessage);
            

            if ( msg.key.remoteJid.includes('@s.whatsapp.net')) {
                if (msg.message) {
                      
                    const buttons = [
                        {buttonId: 'id1', buttonText: {displayText: 'Penting!'}, type: 1},
                        {buttonId: 'id2', buttonText: {displayText: 'Kabar?'}, type: 1},
                      ]
                      
                    if (msg.message.conversation) {
                        await sack.sendMessage(msg.key.remoteJid, {
                            text : `#Ini Bot Trial Pendamping\n\nSelamat datang kak ${msg.pushName}.\n\n Kami akan mendampingi kakak sementara.\n\n Kamu Nanya?`,
                            buttons : buttons
                        },)
                        msg.message.buttonsResponseMessage  = {response : 'response-null'}
                    }

                    if (msg.message.buttonsResponseMessage.selectedDisplayText === 'Penting!') {
                        await sack.sendMessage(msg.key.remoteJid, {
                            text : 'Silakan tulis pesan anda. beberapa saat lagi user akan membalas',
                        },)
                    }  
                    
                    if (msg.message.buttonsResponseMessage.selectedDisplayText === 'Kabar?') {
                        await sack.sendMessage(msg.key.remoteJid, {
                            text : 'Alhamdulillah. Doakan saja dalam keadaan baik baik saja',
                        },)
                    } 
                    
                    
                    
                    


                    // if (msg.message.conversation == 'cek status') {
                    //      // cek ke api
                    //     const url =  "https://script.google.com/macros/s/AKfycbyXzmky2WixRhEA4sy1DTXeLXyinZVPJvvV0UJRIojVyeNYTVnkM39RbrN11yZnr395BA/exec?whatsapp="+msg.key.remoteJid.replace('@s.whatsapp.net', '')
                        
                    //     try {
                    //        let response = await axios.get(url);
                    //        console.log(response.data); 
                    //        const {success, data, message } = response.data
                    //        let kalimat;
                    //        if (success) {
                    //           kalimat = `Hallo kak ${msg.pushName} ${data.nama}`
                    //           await sack.sendMessage(msg.key.remoteJid, {
                    //             text : kalimat
                    //           })
                    //         }
                    //     } catch (error) {
                    //        console.log(error);
                    //     }
                       
                      
                    // } else {
                    //     await sack.sendMessage(msg.key.remoteJid, {
                    //         text : 'Selamat datang kak. \n\n Ini Botnya Ichal. \n\n Kamu Nanya?'
                    //     })
                    // }
                }
            }
        }
        // console.log(JSON.stringify(message));
        // console.log("COBA 1 : "+message.key.remoteJid);
        // console.log("COBA 2 : "+message.message.conversation);

    });
}

startSock();